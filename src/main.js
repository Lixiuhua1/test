// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import 'reset-css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css' 
import {http} from './http'
import filters from './filters'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$http = http


//1
Object.keys(filters).forEach(item=>{
  Vue.filter(item,filters[item])
})


//2
/* for(let key in filters){
  Vue.filter(key,filters[key])
} */

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
